use crate::command::command::Command;

use std::io;

use serenity::client::Context;
use serenity::model::{channel::Message, user::User};
use serenity::utils::Colour;

use async_trait::async_trait;
#[derive(Debug, Clone)]
struct GuildInfo {
    icon: Option<String>,
    member_count: u64,
    guild_name: String,
    owner: User,
}

pub struct About {}

#[async_trait]
impl Command for About {
    async fn cmd(&self) -> &str {
        "about"
    }
    async fn help(&self) -> &str {
        "information"
    }
    async fn execute(&self, ctx: &Context, msg: &Message) -> io::Result<()> {
        let cur_guild = msg.guild(&ctx).await.unwrap();
        let guild_info = GuildInfo {
            icon: cur_guild.clone().icon_url(),
            member_count: cur_guild.clone().member_count,
            guild_name: cur_guild.clone().name,
            owner: cur_guild.clone().owner_id.to_user(&ctx).await.unwrap(),
        };

        let _ = &msg
            .channel_id
            .send_message(&ctx, |m| {
                m.embed(|e| {
                    e.color(Colour::from_rgb(125, 53, 105));
                    e.author(|a| match guild_info.clone().icon {
                        Some(url) => {
                            a.icon_url(url);
                            a.name(guild_info.clone().guild_name)
                        }
                        None => a.name(guild_info.clone().guild_name),
                    });
                    e.field("Member Count", guild_info.clone().member_count, true);
                    e.field("Guild Owner", guild_info.clone().owner.name, true);
                    e.field("Roles", cur_guild.clone().roles.len().to_string(), true);
                    e.field(
                        "Channels",
                        cur_guild.clone().channels.len().to_string(),
                        true,
                    );
                    e.field("Emojis", cur_guild.clone().emojis.len().to_string(), true);

                    e.timestamp(&msg.timestamp)
                })
            })
            .await;
        Ok(())
    }
}
