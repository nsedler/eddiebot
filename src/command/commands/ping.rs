use crate::command::command::Command;

use std::io;

use serenity::client::Context;
use serenity::model::channel::Message;

use async_trait::async_trait;

pub struct Ping {}

#[async_trait]
impl Command for Ping {
    async fn cmd(&self) -> &str {
        "ping"
    }
    async fn help(&self) -> &str {
        "Basic ping command"
    }
    async fn execute(&self, ctx: &Context, msg: &Message) -> io::Result<()> {
        msg.channel_id
            .say(&ctx.http, "Pong!")
            .await
            .expect("testing");
        Ok(())
    }
}
