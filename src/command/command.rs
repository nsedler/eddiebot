use std::io;

use serenity::client::Context;
use serenity::model::channel::Message;

use async_trait::async_trait;

///
/// This is our Command trait
/// It will be the host for creating our commands
/// Each command should have a:
/// `cmd() -> &str` This will be the name of the command
/// `help() -> &str` This will provite information about the command
/// `execute(&Context, &Message) -> io::Result<()>` This will be the function that determines the commands behavior
///
#[async_trait]
pub trait Command {
    async fn cmd(&self) -> &str;
    async fn help(&self) -> &str;
    async fn execute(&self, ctx: &Context, msg: &Message) -> io::Result<()>;
}
