extern crate dotenv;

use crate::command::command::Command;

use serenity::async_trait;
use serenity::client::{Context, EventHandler};
use serenity::model::prelude::{Message, Ready};
use serenity::Client;

use dotenv::dotenv;

mod command;
mod log;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        // A vector of our commands
        let cmds_vec: Vec<Box<dyn Send + Sync + Command>> = vec![
            Box::new(command::commands::ping::Ping {}),
            Box::new(command::commands::general::about::About {}),
        ];

        // Goes through each command in `cmds_vec` and checks if
        // the message is a command
        for cmd in cmds_vec.iter() {
            if msg.content == format!("!{}", &cmd.cmd().await) {
                let cur_command = log::CommandLog {
                    cmd: String::from(cmd.cmd().await),
                    guild: msg.guild(&ctx).await.unwrap(),
                };
                println!("{}", cur_command);
                cmd.execute(&ctx, &msg).await.unwrap();
            }
        }
    }

    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is up and running", ready.user.name);
    }
}

#[tokio::main]
async fn main() {
    // Experimental thread for input
    // Maybe we can use this to
    std::thread::spawn(move || loop {
        use std::io;
        let mut input = String::new();
        io::stdin().read_line(&mut input).unwrap();
        println!("{}", input.split_whitespace().collect::<String>())
    });

    dotenv().ok();
    let token = dotenv::var("TOKEN").expect("err getting token");
    let mut client = Client::new(&token)
        .event_handler(Handler)
        .await
        .expect("error at creating a new client");
    client.start().await.expect("error at client.start");
}
