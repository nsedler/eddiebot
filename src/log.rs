use serenity::model::guild::Guild;

use std::fmt;

///
/// CommandLog
/// This will be our way of logging when commands are used
/// We will want to log where the command was invoked, who invoked it, and invocation count
/// An examle of how this will be display is:
/// `cmd was invoked for the nth time in the test guild`
///
pub struct CommandLog {
    pub cmd: String,
    pub guild: Guild,
}

impl fmt::Display for CommandLog {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "[Command] {} was invoked in the {} guild",
            &self.cmd, &self.guild.name,
        )
    }
}
